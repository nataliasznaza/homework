#!/bin/bash

# pętla if sprawdza czy pierwszy parametr to -h lub --help
# jeśli tak to wyświetla pomoc
if test "$1" = "-h" || test "$1" = "--help"
then
	 echo "Witaj,"
	 echo "Skrypt służy do zmiany wielkości liter nazw plików"
	 echo "w katalogu, w którym się znajduje"
	 echo
	 echo "Sposób wywołania:"
	 echo "------------------"
	 echo " ./zadanie2.sh lower upper"
	 echo "jeśli zmiana małych liter na wielkie"
	 echo " ./zadanie2.sh upper lower"
	 echo "jeśli zmiana wielkich liter na małe"
	 echo "UWAGA!!"
	 echo "------------------"
	 echo "Jeśli doszło do zamiany małych liter na wielkie, to aby ponownie zamienić"
	 echo "i powrócić do małych należy wpisać:"
	 echo "./ZADANIE2.SH upper lower"
	 echo "Jeżeli nie działa należy ponownie nadać uprawnienia do wykonywania pliku"
	 echo "komendą:"
	 echo "         chmod +x ZADANIE2.SH"
	 echo "następnie ponownie wpisać:"
	 echo "./ZADANIE2.SH upper lower"
	 echo "-----------------"
fi

# warunek sprawdza parametr, jeśli nie jest wprowadzony żaden
# to wyświetla komunikat
        if test $# -eq 0
 then
         echo Nie podales parametrow
# warunek sprawdza, czy nie wprowadzono więcej niż 3 parametry
# jeśli jest więcej to wyświetla komunikat
         elif test $# -gt 3
 then
        echo Zle parametry!
fi
#sprawdza wartość parametru 1. i 2.
      if test "$1" = "lower" && test "$2" = "upper"
then
# wykonuje pętlę gdzie dla każdego pliku w katalogu zmienia nazwę 
# z małych liter na wielkie (przenosi nazwę pliku do tego samego katalogu
#przy jednoczesnej zmianie nazwy)
                for file in * ; do
             mv "$file" "$(echo $file | tr [:lower:] [:upper:])";
                done
# sprawdza wartość parametru 1. i 2.
        elif test "$1" = "upper" && test "$2" = "lower"
then

# wykonuje pętlę gdzie dla każdego pliku w katalogu zmienia nazwę
# z wielkich liter na małe ((przenosi nazwę pliku do tego samego katalogu
#przy jednoczesnej zmianie nazwy)
                for file in * ; do
                mv "$file" "$(echo $file | tr [:upper:] [:lower:])";
                done

fi

