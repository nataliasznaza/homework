#!/bin/bash
# pętla if sprawdza czy pierwszy parametr to -h lub --help
# jeśli tak to wyświetla pomoc
if test "$1" = "-h" || test "$1" = "--help"
then

	echo "Witaj,"
	echo "Skrypt zmienia nazwy plików znajdujących się w bieżącym katalogu"
	echo "na nazwę zaczynającą się od kolejnego numeru + podkreślenie"
	echo "Aby wywołać skrypt należy wpisać:"
	echo "------------------"
	echo "./zadanie3.sh change"
	echo "lub"
	echo "sh zadanie3.sh change"
	echo "-----------------"
	echo "Wynikiem działania jest zmiana wszystkich nazw plików" 
	echo "na zaczynające się od kolejnego numeru + podreślenie"
	echo "Aby wyświetlić pomoc należy wpisać:"
	echo "./zadanie3.sh -h lub ./zadanie3.sh --help"

fi
#pętla if sprawdza czy wprowadzono parametr, jeśli nie to wyświetla komunikat
if [ $# -ne 1 ]
then
	echo "Nie podales parametrow"
fi
#zadaję zmienną licznik

LICZNIK=1

#pętla if sprawdza czy pierwszy parametr to słowo change jeśli tak to wykonuje
#dalej pętlę for

if test $1="change"
then

#pętla for zmienia dla każdego pliku w katalogu
	for file in * ; do

#zadaję zmienną new która dodaje do nazwy pliku zmienną LICZNIK oraz ","
# a następnie zamienia ten "," na znak podkreślenia "_"
    	NEW=`echo "$LICZNIK,$file" | tr -s ',' | tr ',' '_'`

#następnie wykonuje zmianę nazwy pliku na NEW zadaną powyżej	
   	 mv "$file" "$NEW"

#następnie następuje zwiększenie licznika o 1
#i wykonanie pętli ponownie
	let LICZNIK++
done
fi



