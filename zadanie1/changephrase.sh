#!/bin/bash

# pętla if sprawdza czy pierwszy parametr to -h lub --help
# jeśli tak to wyświetla pomoc
if test "$1" = "-h" || test "$1" = "--help"
then
	echo "Witaj,"
	echo "Skrypt służy do zmiany wybranej frazy w pliku na inną"
	echo "również wybraną w plikach znajdujących się w bieżącym katalogu"
       	echo "i jego podkatalogach"
	echo "Aby wywołać skrypt należy umieścić go w katalogu,"
	echo "w którym chcemy dokonać zmian w plikach, a następnie:"
	echo "------------------"
	echo "Wpisać w terminal:"
	echo "./changephrase.sh lub sh changephrase.sh"
	echo "potem zostaniesz poproszony o wpisanie FRAZY KTÓRĄ CHCESZ ZMIENIĆ"
	echo "po wpisaniu tej frazy i zatwierdzeniu przez enter"
	echo "zostaniesz poproszony o wpisanie FRAZY NA JAKĄ CHCESZ ZMIENIĆ"
	echo "po wpisaniu tej frazy i zatwierdzeniu przez enter"
	echo "skrypt zamieni wybraną przez Ciebie frazę na inną,"
	echo "również przez Ciebie wybraną w bieżącym katalogu i jego podkatalogach"
	echo "-----------------"
	fi
#zadaję zmienne FRAZA1, którą chcemy zmienić
# oraz FRAZA2 na ktorą chcemy zmienić
FRAZA1="abc"
FRAZA2="xyz"

#prosi użytkownika o podanie frazy i je odczytuje
echo "Podaj jaką frazę chcesz zmienić: "
read FRAZA1
echo "Podaj na jaką frazę chcesz zmienić: "
read FRAZA2

#pętla for szuka plików typu -f i jesli znajduje to wykonuje petle 
	for file in $(find . -type f); do

#petla if wykonuje zamianę FRAZY1 na FRAZE2 w plikach ktore należą do bieżącego
#katalogu i w podkatalogach
      if [[ -f $file ]] && [[ -w $file ]]; then
              sed -i -- "s/$FRAZA1/$FRAZA2/g" "$file"
      fi
      done

